const { singletons } = require('@openzeppelin/test-helpers')

const PaymentTokenMockContract = artifacts.require("PaymentTokenMock")
const RegistryContract = artifacts.require("Registry")
const AragonProtocolMockContract = artifacts.require("AragonProtocolMock")
const TreasuryMockContract = artifacts.require("TreasuryMock")
const MaliciousTokenMockContract = artifacts.require("MaliciousTokenMock")
const MaliciousTokenRecieverMockContract = artifacts.require("MaliciousTokenRecieverMock")

module.exports = async(deployer, network, accounts) => {
    try {
        await deployer.deploy(RegistryContract)

        if (network === 'develop' || network === 'soliditycoverage') {
            const paymentToken = await deployer.deploy(PaymentTokenMockContract, accounts.length == 1 ? accounts[0] : accounts[1])

            await deployer.deploy(AragonProtocolMockContract, paymentToken.address, '100000000')

            const registryContract = await RegistryContract.deployed()
            await deployer.deploy(TreasuryMockContract, registryContract.address)

            await singletons.ERC1820Registry(accounts[0])
            await deployer.deploy(MaliciousTokenMockContract, [registryContract.address])

            await deployer.deploy(MaliciousTokenRecieverMockContract)
        }
    } catch (err) {
        console.error(err)
    }
}
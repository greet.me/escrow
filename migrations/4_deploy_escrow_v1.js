const RegistryContract = artifacts.require("Registry")
const EscrowContract = artifacts.require("EscrowV1")

module.exports = async(deployer, network, accounts) => {
    try {
        const registryContract = await RegistryContract.deployed()

        const escrowContract = await deployer.deploy(EscrowContract, registryContract.address)
        await registryContract.toggleContractRegistration(escrowContract.address, true)
    } catch (err) {
        console.error(err)
    }
}
const path = require("path");

module.exports = {
    mode: 'development',
    entry: {
        server: "./src/server.js",
        greet_escrow: "./src/index.js",
    },
    output: {
        filename: "[name].js",
        sourceMapFilename: "[name].js.map",
        path: path.resolve(__dirname, "dist"),
        library: 'GreetEscrow',
        libraryTarget: 'commonjs'
    },
    devServer: { contentBase: path.join(__dirname, "dist"), compress: true },
    devtool: "source-map"
};
import DaiERC20 from '../vendor/DaiERC20'
import AragonTokenManager from '../vendor/AragonTokenManager'
import { ethers, utils } from 'ethers'
import { ethersProvider } from './config'
import escrowV1Artifact from '../../build/contracts/EscrowV1.json'
import { hash as namehash } from 'eth-ens-namehash'

// These app IDs are generated from <name>.aragonpm.eth
const KNOWN_APPS = {
  Agent: namehash('agent.aragonpm.eth'),
  Finance: namehash('finance.aragonpm.eth'),
  Fundraising: namehash('aragon-fundraising.aragonpm.eth'),
  Survey: namehash('survey.aragonpm.eth'),
  TokenManager: namehash('token-manager.aragonpm.eth'),
  Vault: namehash('vault.aragonpm.eth'),
  Voting: namehash('voting.aragonpm.eth'),
}

const FETCH_ORG_QUERY = `query Apps($address: Bytes!) {
  apps(where: {address: $address}){ 
    organization {
      apps {
        appId
        address
      }
    }
  }
}`

const ORGANIZATION_INFO = 'ORGANIZATION_INFO&'
const GQL_ENDPOINT = {
  1: 'https://api.thegraph.com/subgraphs/name/aragon/aragon-mainnet',
  4: 'https://api.thegraph.com/subgraphs/name/aragon/aragon-rinkeby'
}

const CALL_CODE = '00000001'
const FORWARD_SIG = 'd948d468'
const AGENT_EXEC_SIG = 'b61d27f6'
const VOTE_SIG = 'f4b00513'
const AGENT_SEPARATOR = '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000060'

const votingInterface = new utils.Interface([
  "function newVote(bytes _executionScript, string _metadata, bool _castVote, bool _executesIfDecided)",
  "function forward(bytes _evmScript)"
])

function toAbiInt(number) {
  return utils.defaultAbiCoder.encode(["uint256"], [number]).toString('hex')
}

function getAbiLength(hexContent, extra = 0) {
  return (hexContent.length / 2 + extra).toString(16).padStart(8, '0')
}

function getTokenManagerContract(address) {
  return new ethers.Contract(
    address,
    AragonTokenManager.abi,
    ethersProvider()
  )
}

const getOrganizationByAddress = async function(networkId, address) {
  const LOCAL_STORAGE_KEY = `${ORGANIZATION_INFO}${networkId}&${address.toLowerCase()}`
  if (localStorage.getItem(LOCAL_STORAGE_KEY)) {
    return JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
  }

  const data = await fetch(GQL_ENDPOINT[networkId], {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({
      query: FETCH_ORG_QUERY,
      variables: { address: address.toLowerCase() },
    }),
  })

  if (data.ok) {
    const json = await data.json()
    if (
      json &&
      json.data.apps &&
      json.data.apps.length === 1
    ) {
      const organization = json.data.apps[0].organization
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(organization))
      return organization
    }
  }
  return null
}

function getOrgAppsByAddress(networkId, address) {
  return getOrganizationByAddress(networkId, address)
    .then((data) => {
      if (!data) throw 'invalid Agent app address'

      const tokenManagerAddress = data.apps.find(({ appId }) => appId === KNOWN_APPS.TokenManager).address
      const votingAppAddress = data.apps.find(({ appId }) => appId === KNOWN_APPS.Voting).address
      const agentAppAddress = data.apps.find(({ appId }) => appId === KNOWN_APPS.Agent).address
      return { 
        votingAppAddress,
        agentAppAddress,
        tokenManagerAddress
      }
    })
}

function encodeExecuteScript (actions, agentAppAddress) {
  return actions.reduce((script, { to, data }) => {
    const prefix = agentAppAddress.toLowerCase().slice(2) + 
      getAbiLength(data, -1 + 32 + 64 + 36) + AGENT_EXEC_SIG

    const address = utils.defaultAbiCoder.encode(["address"], [to]).slice(2)
    const dataLength = toAbiInt((data.length - 2) / 2)
    return script + prefix + address + AGENT_SEPARATOR + dataLength.slice(2) + data.slice(2)
  }, CALL_CODE)
}

function generateApproveSignAndFundEvmScript(
  networkId,
  { 
    votingAppAddress,
    agentAppAddress,
    tokenAddress,
    amount,
    milestoneData
  },
  approved
) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  const operation1 = new utils.Interface(DaiERC20.abi)
    .encodeFunctionData("approve", [escrowAddress, '0'])
  const operation2 = new utils.Interface(DaiERC20.abi)
    .encodeFunctionData("approve", [escrowAddress, amount])
  const operation3 = new utils.Interface(escrowV1Artifact.abi)
    .encodeFunctionData("signAndFundMilestone", milestoneData)

  let agentScript
  if (approved.eq(ethers.BigNumber.from("0"))) {
    agentScript = encodeExecuteScript([
      {
        to: tokenAddress,
        data: operation2
      },
      {
        to: escrowAddress,
        data: operation3
      }
    ], agentAppAddress)
  } else if (approved.gte(ethers.BigNumber.from(data.amount))) {
    agentScript = encodeExecuteScript([
      {
        to: escrowAddress,
        data: operation3
      }
    ], agentAppAddress)
  } else {
    agentScript = encodeExecuteScript([
      {
        to: tokenAddress,
        data: operation1
      },
      {
        to: tokenAddress,
        data: operation2
      },
      {
        to: escrowAddress,
        data: operation3
      }
    ], agentAppAddress)
  }

  return voteEvmScript(votingAppAddress, agentScript, milestoneData[0])
}

function callBatchedSignAndFundVote(networkId, agentAppAddress, data) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  const ERC20 = new ethers.Contract(data.tokenAddress, DaiERC20.abi, ethersProvider())
  return Promise.all([getOrgAppsByAddress(networkId, agentAppAddress), ERC20.allowance(agentAppAddress, escrowAddress)])
    .then(([appAddresses, approved]) => {
      const params = Object.assign(data, appAddresses)
      const payload = generateApproveSignAndFundEvmScript(networkId, params, approved)
      return callForward(appAddresses.tokenManagerAddress, payload)
    })
}

function generateApproveFundEvmScript(
  networkId,
  { 
    votingAppAddress,
    agentAppAddress,
    tokenAddress,
    amount,
    milestoneData
  },
  approved
) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  const operation1 = new utils.Interface(DaiERC20.abi)
    .encodeFunctionData("approve", [escrowAddress, '0'])
  const operation2 = new utils.Interface(DaiERC20.abi)
    .encodeFunctionData("approve", [escrowAddress, amount])
  const operation3 = new utils.Interface(escrowV1Artifact.abi)
    .encodeFunctionData("fundMilestone", milestoneData)

  let agentScript
  if (approved.eq(ethers.BigNumber.from("0"))) {
    agentScript = encodeExecuteScript([
      {
        to: tokenAddress,
        data: operation2
      },
      {
        to: escrowAddress,
        data: operation3
      }
    ], agentAppAddress)
  } else if (approved.gte(ethers.BigNumber.from(data.amount))) {
    agentScript = encodeExecuteScript([
      {
        to: escrowAddress,
        data: operation3
      }
    ], agentAppAddress)
  } else {
    agentScript = encodeExecuteScript([
      {
        to: tokenAddress,
        data: operation1
      },
      {
        to: tokenAddress,
        data: operation2
      },
      {
        to: escrowAddress,
        data: operation3
      }
    ], agentAppAddress)
  }

  return voteEvmScript(votingAppAddress, agentScript, milestoneData[0])
}

function callBatchedFundVote(networkId, agentAppAddress, data) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  const ERC20 = new ethers.Contract(data.tokenAddress, DaiERC20.abi, ethersProvider())
  return Promise.all([getOrgAppsByAddress(networkId, agentAppAddress), ERC20.allowance(agentAppAddress, escrowAddress)])
    .then(([appAddresses, approved]) => {
      const params = Object.assign(data, appAddresses)
      const payload = generateApproveFundEvmScript(networkId, params, approved)
      return callForward(appAddresses.tokenManagerAddress, payload)
    })
}

function generateSignAndProposeEvmScript(
  networkId,
  { 
    votingAppAddress,
    agentAppAddress,
    contractData
  }
) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  const operation1 = new utils.Interface(escrowV1Artifact.abi)
    .encodeFunctionData("signAndProposeContractVersion", contractData)

  const agentScript = encodeExecuteScript([
    {
      to: escrowAddress,
      data: operation1
    }
  ], agentAppAddress)

  return voteEvmScript(votingAppAddress, agentScript, contractData[0])
}

function callSignAndProposeVote(networkId, agentAppAddress, data) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  return Promise.all([getOrgAppsByAddress(networkId, agentAppAddress)])
    .then(([appAddresses]) => {
      const params = Object.assign(data, appAddresses)
      const payload = generateSignAndProposeEvmScript(networkId, params)
      return callForward(appAddresses.tokenManagerAddress, payload)
    })
}

function generateSignAndApproveForPayeeDaoEvmScript(
  networkId,
  { 
    votingAppAddress,
    agentAppAddress,
    tokenAddress,
    amount,
    contractData,
    milestoneData
  },
  approved
) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  const operation1 = new utils.Interface(DaiERC20.abi)
    .encodeFunctionData("approve", [escrowAddress, '0'])
  const operation2 = new utils.Interface(DaiERC20.abi)
    .encodeFunctionData("approve", [escrowAddress, amount])
  const operation3 = new utils.Interface(escrowV1Artifact.abi)
    .encodeFunctionData("signAndApproveContractVersion", contractData)
  const operation4 = new utils.Interface(escrowV1Artifact.abi)
    .encodeFunctionData("fundMilestone", milestoneData)

  let agentScript
  if (approved.eq(ethers.BigNumber.from("0"))) {
    agentScript = encodeExecuteScript([
      {
        to: tokenAddress,
        data: operation2
      },
      {
        to: escrowAddress,
        data: operation3
      },
      {
        to: escrowAddress,
        data: operation4
      }
    ], agentAppAddress)
  } else if (approved.gte(ethers.BigNumber.from(data.amount))) {
    agentScript = encodeExecuteScript([
      {
        to: escrowAddress,
        data: operation3
      },
      {
        to: escrowAddress,
        data: operation4
      }
    ], agentAppAddress)
  } else {
    agentScript = encodeExecuteScript([
      {
        to: tokenAddress,
        data: operation1
      },
      {
        to: tokenAddress,
        data: operation2
      },
      {
        to: escrowAddress,
        data: operation3
      },
      {
        to: escrowAddress,
        data: operation4
      }
    ], agentAppAddress)
  }

  return voteEvmScript(votingAppAddress, agentScript, milestoneData[0])
}

function callBatchedSignAndFundForPayeeDaoVote(networkId, agentAppAddress, data) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  const ERC20 = new ethers.Contract(data.tokenAddress, DaiERC20.abi, ethersProvider())
  return Promise.all([getOrgAppsByAddress(networkId, agentAppAddress), ERC20.allowance(agentAppAddress, escrowAddress)])
    .then(([appAddresses, approved]) => {
      const params = Object.assign(data, appAddresses)
      const payload = generateSignAndApproveForPayeeDaoEvmScript(networkId, params, approved)
      return callForward(appAddresses.tokenManagerAddress, payload)
    })
}

function callForward(tokenManagerAddress, payload) {
  const signer = ethersProvider().getSigner()
  const tokenManagerContract = getTokenManagerContract(tokenManagerAddress)
  const tokenManagerSigner = tokenManagerContract.connect(signer)
  return tokenManagerSigner.forward(payload)
}

function voteEvmScript(votingAppAddress, agentScript, cid) {
  const desc = `ipfs:${utils.base58.encode("0x1220" + cid.slice(2))}`

  // Metadata is currently not used for Agent votes
  const voteScript = votingInterface.encodeFunctionData("newVote", ["0x" + agentScript, desc, false, false]).slice(2)
  //const voteScript = votingInterface.encodeFunctionData("forward", ["0x" + agentScript]).slice(2)

  const prefix = "0x" +
    // First 64 bytes forwarder to voting app
    CALL_CODE + votingAppAddress.toLowerCase().slice(2) + getAbiLength(voteScript)
  
  return `${prefix}${voteScript}`
}


// We check if it's possible to create a trivial vote with ERC20 approval
function checkVotingRights(networkId, appAddress) {
  const escrowAddress = escrowV1Artifact.networks[`${networkId}`].address
  return getOrgAppsByAddress(networkId, appAddress)
    .then(({ votingAppAddress, agentAppAddress, tokenManagerAddress }) => {
      const operation1 = new utils.Interface(DaiERC20.abi)
        .encodeFunctionData("approve", [escrowAddress, '0'])

      const agentScript = encodeExecuteScript([
        {
          to: escrowAddress,
          data: operation1
        }
      ], agentAppAddress)

      const voteScript = votingInterface.encodeFunctionData("forward", ["0x" + agentScript]).slice(2)
      const prefix = "0x" +
        // First 64 bytes forwarder to voting app
        CALL_CODE + votingAppAddress.toLowerCase().slice(2) + getAbiLength(voteScript)
      const payload = `${prefix}${voteScript}`
      
      const signer = ethersProvider().getSigner()
      const tokenManagerContract = getTokenManagerContract(tokenManagerAddress)
      const tokenManagerSigner = tokenManagerContract.connect(signer)
      return tokenManagerSigner.estimateGas.forward(payload)
    })
}

export default {
  getOrganizationByAddress,
  getOrgAppsByAddress,
  generateApproveSignAndFundEvmScript,
  callBatchedSignAndFundVote,
  callBatchedFundVote,
  callSignAndProposeVote,
  callBatchedSignAndFundForPayeeDaoVote,
  callForward,
  checkVotingRights
}

import { utils } from 'ethers'

function isEthereumAddress(str) {
    return str.match(/^0x\w{40}$/) !== null
}

function bytes32Cid(cid) {
    return utils.hexlify(utils.base58.decode(cid).slice(2))
}

function genMid(cid, index) {
    return utils.keccak256(utils.defaultAbiCoder.encode(['bytes32','uint16'], [cid, index]))
}

function genAuid(cid, amendmentCid) {
    return utils.keccak256(utils.defaultAbiCoder.encode(['bytes32','bytes32'], [cid, amendmentCid]))
}

function debounce(callback, wait, immediate = false) {
    let timeout = null

    return function() {
        const callNow = immediate && !timeout
        const next = () => callback.apply(this, arguments)

        clearTimeout(timeout)
        timeout = setTimeout(next, wait)

        if (callNow) {
            next()
        }
    }
}

export default {
    isEthereumAddress,
    bytes32Cid,
    genMid,
    genAuid,
    debounce
}
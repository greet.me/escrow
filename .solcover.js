module.exports = {
  skipFiles: [
    'Migrations.sol',
    'mocks/TreasuryMock.sol',
    'mocks/InsuranceManagerMock.sol',
    'mocks/PaymentTokenMock.sol',
    'mocks/AragonProtocolMock.sol',
    'mocks/MaliciousDisputerMock.sol',
    'mocks/MaliciousTokenMock.sol',
    'mocks/MaliciousTokenRecieverMock.sol'
  ],
  providerOptions: {
    allowUnlimetedContractSize: true,
    gasLimit: 0xfffffffffff
  }
};
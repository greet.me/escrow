// SPDX-License-Identifier: MPL-2.0

pragma solidity >=0.8.4 <0.9.0;

import "../interfaces/IEscrow.sol";
import "../../node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../../node_modules/@openzeppelin/contracts/token/ERC777/ERC777.sol";

contract MaliciousTokenMock is ERC777 {
    IEscrow public escrow;
    bytes32 public cid;
    uint16 public index;
    bool public initialized = false;
    bool attackDone = true;
    uint256 attackType = 0;
    bytes attackSign;
    uint attackAmount;

    event Attacked();

    constructor(address[] memory defaultOperators) ERC777("Evil", "EVL", defaultOperators) {
    }

    function initialize(address _escrow, uint16 _index, bytes32 _cid) external {
        escrow = IEscrow(_escrow);
        index = _index;
        cid = _cid;
        initialized = true;
        attackDone = true;
    }

    function prepareAttack(uint256 _type, uint _amount, bytes calldata _sign) public {
        attackDone = false;
        attackType = _type;
        attackSign = _sign;
        attackAmount = _amount;
    }

    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        if (!attackDone && attackType > 0) {
            emit Attacked();
            attackDone = true;
            if (attackType == 1) escrow.withdrawMilestone(cid, index);
            if (attackType == 2) escrow.withdrawPreApprovedMilestone(cid, index, attackAmount, attackSign);
            if (attackType == 3) escrow.refundMilestone(cid, index);
            if (attackType == 4) escrow.refundPreApprovedMilestone(cid, index, attackAmount, attackSign);
        }
        return super.transfer(recipient, amount);
    }

    function faucet() external {
        _mint(msg.sender, 250000000000, "", "");
    }

    function signAndFundMilestone(bytes32 id, uint16 ind, bytes32 terms, uint amount, bytes calldata sig) external {
        IERC20(address(this)).approve(address(escrow), amount);
        escrow.signAndFundMilestone(id, ind, terms, amount, sig, "");
    }
}
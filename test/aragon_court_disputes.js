const { assert } = require('chai')
const truffleAssert = require('truffle-assertions')
const { GreetEscrow } = require('../js/dist/server')
const { utils } = require('../js/node_modules/ethers')

const Registry = artifacts.require('Registry')
const EscrowDisputeManager = artifacts.require('AragonCourtDisputerV1')
const EscrowForAragon = artifacts.require('EscrowV1')
const PaymentToken = artifacts.require('PaymentTokenMock')
const AragonProtocol = artifacts.require('AragonProtocolMock')

const RULE_PAYEE_WON = '3';
const RULE_PAYER_WON = '4';

const CONTRACT_CID = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a010b6' //'Qmb3u8PnDeU3stUeo68d6HXhjpmBEU71tJwsBeFreDezfo'
const EVIDENCE2_CID = '0xcf6a209306b7d4bd9041a8d1d4b25577d503b7bc06313166d8dbd5ffe237a9ee' //'QmcJJxmvWZP7Xvz8ZccYUi6wMMFPETX7Xe16AyRPYAUj2d'
const EVIDENCE3_CID = '0x29f2d17be6139079dc48696d1f582a8530eb9805b561eda517e22a892c7e3f1f' //'QmRAQB6YaCyidP37UdDnjFY5vQuiBrcqdyoW1CuDgwxkD4'
const STATEMENT_CID = '0xcf116662909ada21c1ce86fce1b5d388e2edcb08856004fe53c2d17632b85d73' //'QmcGxVTu2BDV5bU9AeLxhTMS6aXDWSL5dN9HDSxNgV5T2J'

const genMid = GreetEscrow.util.genMid

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// State persists sequencially for each test
contract('AragonCourtDisputerV1', accounts => {
    const PAYER = accounts[1]
    const PAYEE = accounts[2]
    const RANDOM = accounts[3]
    const PAYER_DEL = accounts[4]
    const PAYEE_DEL = accounts[5]

    it('should allow registration of new contract', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 3
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 3
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }]
        await escrow.registerContract.sendTransaction(
            CONTRACT_CID,
            PAYER,
            PAYER_DEL,
            PAYEE,
            PAYEE_DEL,
            milestone_params, { from: RANDOM }
        )

        assert.equal(await escrow.lastMilestoneIndex(CONTRACT_CID), 8, "should be 8")

        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            CONTRACT_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 1, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 2, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 3, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 301, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 302, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 4, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 5, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 6, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 7, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 8, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
    })

    it('should not allow dispute from non-party', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.proposeSettlement.sendTransaction(
                CONTRACT_CID,
                1,
                80,
                20,
                STATEMENT_CID, { from: RANDOM }
            ),
            null,
            "Not a disputer"
        );
    })

    it('should allow dispute from payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 1, 100, 0, STATEMENT_CID, { from: PAYER })
        await sleep(2000) // should pass at least 1 second from first suggestion
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 1, true, { from: PAYER })
        await escrow.submitEvidence.sendTransaction(CONTRACT_CID, 1, EVIDENCE2_CID, { from: PAYEE })
    })

    it('should not allow evidences from non-party', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.submitEvidence.sendTransaction(
                CONTRACT_CID,
                1,
                EVIDENCE3_CID, { from: RANDOM }
            ),
            null,
            "Not a disputer"
        );
    })

    it('should not allow resubmittion for ongoing dispute', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.proposeSettlement.sendTransaction(
                CONTRACT_CID,
                1,
                20,
                80,
                STATEMENT_CID, { from: PAYEE }
            ),
            null,
            "In dispute"
        );
    })

    it('show allow resolution from anyone', async() => {
        const escrow = await EscrowForAragon.deployed()
        const protocol = await AragonProtocol.deployed()
        const paymentToken = await PaymentToken.deployed()
        await protocol.setRulingForDispute(1, RULE_PAYER_WON, { from: RANDOM }) // dispute indexation starts from 1

        const balance1 = await paymentToken.balanceOf(PAYER);
        await escrow.executeSettlement(CONTRACT_CID, 1, { from: RANDOM })
        await escrow.refundMilestone(CONTRACT_CID, 1, { from: RANDOM })

        milestone1 = await escrow.milestones.call(genMid(CONTRACT_CID, 1), { from: RANDOM })
        assert.equal(milestone1.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone1.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone1.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone1.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone1.claimedAmount.toString(), '50000000', "should be 50")
        const balance2 = await paymentToken.balanceOf(PAYER);
        assert.equal(balance2.sub(balance1).toString(), '50000000', "should be 50")
    })

    it('should not allow resubmittion for resolved dispute', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.proposeSettlement.sendTransaction(
                CONTRACT_CID,
                1,
                70,
                30,
                STATEMENT_CID, { from: PAYEE }
            ),
            null,
            "In dispute"
        );
    })

    it('should allow dispute from payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        const protocol = await AragonProtocol.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.transfer(PAYEE, '100000000', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYEE })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 2, 0, 100, STATEMENT_CID, { from: PAYEE })
        await sleep(2000) // should pass at least 1 second from first suggestion
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 2, true, { from: PAYEE })

        await escrow.submitEvidence.sendTransaction(CONTRACT_CID, 2, EVIDENCE2_CID, { from: PAYER })
        await protocol.setRulingForDispute(2, RULE_PAYEE_WON, { from: RANDOM }) // dispute indexation starts from 1

        const balance1 = await paymentToken.balanceOf(PAYEE);
        await escrow.executeSettlement(CONTRACT_CID, 2, { from: PAYEE })
        await escrow.withdrawMilestone(CONTRACT_CID, 2, { from: PAYEE })

        milestone2 = await escrow.milestones.call(genMid(CONTRACT_CID, 2), { from: RANDOM })
        assert.equal(milestone2.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone2.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone2.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone2.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone2.claimedAmount.toString(), '50000000', "should be 50")
        const balance2 = await paymentToken.balanceOf(PAYEE);
        assert.equal(balance2.sub(balance1).toString(), '50000000', "should be 50")
    })

    it('should not allow settlement of child milestones directly', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.proposeSettlement(
                CONTRACT_CID,
                301,
                80,
                20, 
                STATEMENT_CID, { from: PAYER }
            ),
            null,
            "Dispute parent"
        );
    })

    it('should not allow execution of settlement of child milestones if there is no parent ruling', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.executeSettlement(
                CONTRACT_CID,
                301, { from: PAYER }
            ),
            null,
            "Dispute parent"
        );
    })

    it('should allow settlement from payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        const protocol = await AragonProtocol.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.transfer(PAYEE, '100000000', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYEE })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 3, 20, 80, STATEMENT_CID, { from: PAYEE })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 3, 80, 20, STATEMENT_CID, { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 3, 30, 70, STATEMENT_CID, { from: PAYEE })
        await sleep(2000) // should pass at least 1 second from first suggestion
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 3, true, { from: PAYEE })

        // Shouldn't allow propsals after dispute started
        await truffleAssert.fails(
            escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 3, 90, 10, STATEMENT_CID, { from: PAYER }),
            null,
            "In dispute"
        );

        await escrow.submitEvidence.sendTransaction(CONTRACT_CID, 3, EVIDENCE2_CID, { from: PAYER })
        await escrow.submitEvidence.sendTransaction(CONTRACT_CID, 3, EVIDENCE3_CID, { from: PAYEE })
        await protocol.setRulingForDispute(3, RULE_PAYEE_WON, { from: RANDOM })
        
        const balance1 = await paymentToken.balanceOf(PAYEE);
        const balance3 = await paymentToken.balanceOf(PAYER);

        const operation1 = new utils.Interface([
            "function executeSettlement(bytes32 _cid, uint16 _index)"
        ]).encodeFunctionData("executeSettlement", [CONTRACT_CID, 3])
        const operation2 = new utils.Interface([
            "function withdrawMilestone(bytes32 _cid, uint16 _index)"
        ]).encodeFunctionData("withdrawMilestone", [CONTRACT_CID, 3])
        await escrow.multicall.sendTransaction([operation1, operation2], { from: PAYER })
        await escrow.refundMilestone(CONTRACT_CID, 3, { from: RANDOM })

        milestone3 = await escrow.milestones.call(genMid(CONTRACT_CID, 3), { from: RANDOM })
        assert.equal(milestone3.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone3.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone3.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone3.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone3.claimedAmount.toString(), '50000000', "should be 50")
        const balance2 = await paymentToken.balanceOf(PAYEE);
        assert.equal(balance2.sub(balance1).toString(), '35000000', "should be 35")
        const balance4 = await paymentToken.balanceOf(PAYER);
        assert.equal(balance4.sub(balance3).toString(), '15000000', "should be 15")
    })

    it('should allow automatic settlement for child milestones by parent ruling', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const balance1 = await paymentToken.balanceOf(PAYER);
        const balance2 = await paymentToken.balanceOf(PAYEE);
        await escrow.executeSettlement(CONTRACT_CID, 301, { from: PAYER })
        await escrow.withdrawMilestone(CONTRACT_CID, 301, { from: RANDOM })
        await escrow.refundMilestone(CONTRACT_CID, 301, { from: RANDOM })
        const balance3 = await paymentToken.balanceOf(PAYER);
        const balance4 = await paymentToken.balanceOf(PAYEE);
        assert.equal(balance3.sub(balance1).toString(), '15000000', "should be 15")
        assert.equal(balance4.sub(balance2).toString(), '35000000', "should be 35")
    })

    it('should not allow double automatic settlement for child milestones by parent ruling', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.executeSettlement.sendTransaction(CONTRACT_CID, 301, { from: PAYEE }),
            null,
            "Nothing to withdraw"
        );
    })

    it('should not allow invalid settlements', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 4, 10, 80, STATEMENT_CID, { from: PAYEE }),
            null,
            "100% required"
        );
    })

    it('should not allow to dispute settlement before it reaches the maturity date', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.transfer(PAYEE, '100000000', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYEE })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 4, 20, 80, STATEMENT_CID, { from: PAYEE })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 4, 100, 0, STATEMENT_CID, { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 4, 30, 70, STATEMENT_CID, { from: PAYEE })
        await truffleAssert.fails(
            escrow.disputeSettlement.sendTransaction(
                CONTRACT_CID,
                4,
                true, { from: PAYEE }
            ),
            null,
            "Not ready for dispute"
        );
    })

    it('should not allow to dispute valid settlement by non-party', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.transfer(RANDOM, '100000000', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: RANDOM })
        await truffleAssert.fails(
            escrow.disputeSettlement.sendTransaction(
                CONTRACT_CID,
                4,
                true, { from: RANDOM }
            ),
            null,
            "Not a disputer"
        );
    })

    it('should not allow to offer settlement by non-party', async() => {
        const registry = await Registry.deployed()
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const disputer = await EscrowDisputeManager.deployed()

        await paymentToken.transfer(RANDOM, '100000000', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: RANDOM })

        await truffleAssert.fails(
            escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 4, 20, 80, STATEMENT_CID, { from: RANDOM }),
            null,
            "Not a disputer"
        );

        await registry.toggleContractRegistration.sendTransaction(RANDOM, true, { from: accounts[0] })
        await truffleAssert.fails(
            disputer.proposeSettlement.sendTransaction(CONTRACT_CID, 4, RANDOM, PAYER, PAYEE, 20, 80, STATEMENT_CID, { from: RANDOM }),
            null,
            ""
        );
        await registry.toggleContractRegistration.sendTransaction(RANDOM, false, { from: accounts[0] })
    })

    it('should allow settlement from payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        const protocol = await AragonProtocol.deployed()
        const paymentToken = await PaymentToken.deployed()
        await sleep(1000) // should pass at least 1 second from first suggestion
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 4, true, { from: PAYER })
        await protocol.setRulingForDispute(4, RULE_PAYER_WON, { from: RANDOM })
        
        const balance1 = await paymentToken.balanceOf(PAYEE);
        const balance3 = await paymentToken.balanceOf(PAYER);
        await escrow.executeSettlement.sendTransaction(CONTRACT_CID, 4, { from: PAYEE })
        await escrow.refundMilestone(CONTRACT_CID, 4, { from: RANDOM })
        
        milestone4 = await escrow.milestones.call(genMid(CONTRACT_CID, 4), { from: RANDOM })
        assert.equal(milestone4.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone4.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone4.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone4.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone4.claimedAmount.toString(), '50000000', "should be 50")
        const balance2 = await paymentToken.balanceOf(PAYEE);
        assert.equal(balance2.sub(balance1).toString(), '0', "should be 0")
        const balance4 = await paymentToken.balanceOf(PAYER);
        assert.equal(balance4.sub(balance3).toString(), '50000000', "should be 50")
    })

    it('should not allow internal resolution from random parties', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 5, 80, 20, STATEMENT_CID, { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 5, 10, 90, STATEMENT_CID, { from: PAYEE })
        
        await truffleAssert.fails(
            escrow.acceptSettlement.sendTransaction(CONTRACT_CID, 5, { from: RANDOM }),
            null,
            ""
        )
    })

    it('should allow internal resolution from payee to payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await escrow.acceptSettlement.sendTransaction(CONTRACT_CID, 5, { from: PAYEE })

        const balance1 = await paymentToken.balanceOf(PAYEE);
        const balance3 = await paymentToken.balanceOf(PAYER);
        await escrow.executeSettlement.sendTransaction(CONTRACT_CID, 5, { from: PAYEE })
        await escrow.withdrawMilestone(CONTRACT_CID, 5, { from: RANDOM })
        await escrow.refundMilestone(CONTRACT_CID, 5, { from: RANDOM })
        
        milestone5 = await escrow.milestones.call(genMid(CONTRACT_CID, 5), { from: RANDOM })
        assert.equal(milestone5.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone5.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone5.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone5.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone5.claimedAmount.toString(), '50000000', "should be 50")
        const balance2 = await paymentToken.balanceOf(PAYEE);
        assert.equal(balance2.sub(balance1).toString(), '10000000', "should be 10")
        const balance4 = await paymentToken.balanceOf(PAYER);
        assert.equal(balance4.sub(balance3).toString(), '40000000', "should be 40")
    })

    it('should allow internal resolution from payer to payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 6, 80, 20, STATEMENT_CID, { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 6, 10, 90, STATEMENT_CID, { from: PAYEE })
        await escrow.acceptSettlement.sendTransaction(CONTRACT_CID, 6, { from: PAYER })

        const balance1 = await paymentToken.balanceOf(PAYEE);
        const balance3 = await paymentToken.balanceOf(PAYER);
        await escrow.executeSettlement.sendTransaction(CONTRACT_CID, 6, { from: PAYER })
        await escrow.withdrawMilestone(CONTRACT_CID, 6, { from: RANDOM })
        await escrow.refundMilestone(CONTRACT_CID, 6, { from: RANDOM })
        
        milestone6 = await escrow.milestones.call(genMid(CONTRACT_CID, 6), { from: RANDOM })
        assert.equal(milestone6.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone6.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone6.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone6.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone6.claimedAmount.toString(), '50000000', "should be 50")
        const balance2 = await paymentToken.balanceOf(PAYEE);
        assert.equal(balance2.sub(balance1).toString(), '45000000', "should be 45")
        const balance4 = await paymentToken.balanceOf(PAYER);
        assert.equal(balance4.sub(balance3).toString(), '5000000', "should be 5")
    })

    it('should allow internal resolution from payee delegate to payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 7, 80, 20, STATEMENT_CID, { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 7, 10, 90, STATEMENT_CID, { from: PAYEE })
        await escrow.acceptSettlement.sendTransaction(CONTRACT_CID, 7, { from: PAYEE_DEL })

        const balance1 = await paymentToken.balanceOf(PAYEE);
        const balance3 = await paymentToken.balanceOf(PAYER);
        await escrow.executeSettlement.sendTransaction(CONTRACT_CID, 7, { from: PAYEE_DEL })
        await escrow.withdrawMilestone(CONTRACT_CID, 7, { from: RANDOM })
        await escrow.refundMilestone(CONTRACT_CID, 7, { from: RANDOM })
        
        milestone7 = await escrow.milestones.call(genMid(CONTRACT_CID, 7), { from: RANDOM })
        assert.equal(milestone7.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone7.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone7.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone7.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone7.claimedAmount.toString(), '50000000', "should be 50")
        const balance2 = await paymentToken.balanceOf(PAYEE);
        assert.equal(balance2.sub(balance1).toString(), '10000000', "should be 10")
        const balance4 = await paymentToken.balanceOf(PAYER);
        assert.equal(balance4.sub(balance3).toString(), '40000000', "should be 40")
    })

    it('should allow internal resolution from payer delegate to payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 8, 80, 20, STATEMENT_CID, { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 8, 10, 90, STATEMENT_CID, { from: PAYEE })
        await escrow.acceptSettlement.sendTransaction(CONTRACT_CID, 8, { from: PAYER_DEL })

        const balance1 = await paymentToken.balanceOf(PAYEE);
        const balance3 = await paymentToken.balanceOf(PAYER);
        await escrow.executeSettlement.sendTransaction(CONTRACT_CID, 8, { from: PAYER_DEL })
        await escrow.withdrawMilestone(CONTRACT_CID, 8, { from: RANDOM })
        await escrow.refundMilestone(CONTRACT_CID, 8, { from: RANDOM })
        
        milestone8 = await escrow.milestones.call(genMid(CONTRACT_CID, 8), { from: RANDOM })
        assert.equal(milestone8.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone8.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone8.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone8.fundedAmount.toString(), '50000000', "should be 50")
        assert.equal(milestone8.claimedAmount.toString(), '50000000', "should be 50")
        const balance2 = await paymentToken.balanceOf(PAYEE);
        assert.equal(balance2.sub(balance1).toString(), '45000000', "should be 45")
        const balance4 = await paymentToken.balanceOf(PAYER);
        assert.equal(balance4.sub(balance3).toString(), '5000000', "should be 5")
    })
})
const { assert } = require('chai')
const truffleAssert = require('truffle-assertions')
const { GreetEscrow } = require('../js/dist/server')
const { utils } = require('../js/node_modules/ethers')

const Registry = artifacts.require('Registry')
const EscrowDisputeManager = artifacts.require('AragonCourtDisputerV1')
const EscrowForAragon = artifacts.require('EscrowV1')
const PaymentToken = artifacts.require('PaymentTokenMock')
const Treasury = artifacts.require('TreasuryMock')
const InsuranceManager = artifacts.require('InsuranceManagerMock')

const CONTRACT_CID = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a010b6' //'Qmb3u8PnDeU3stUeo68d6HXhjpmBEU71tJwsBeFreDezfo'
const STATEMENT_CID = '0xcf116662909ada21c1ce86fce1b5d388e2edcb08856004fe53c2d17632b85d73' //'QmcGxVTu2BDV5bU9AeLxhTMS6aXDWSL5dN9HDSxNgV5T2J'

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const genMid = GreetEscrow.util.genMid

// State persists sequencially for each test
contract('EscrowV1', accounts => {
    const PAYER = accounts[1]
    const PAYEE = accounts[2]
    const RANDOM = accounts[3]

    it('should allow registration of new contract', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: Treasury.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }]
        await escrow.registerContract.sendTransaction(
            CONTRACT_CID,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestone_params, { from: RANDOM }
        )
    })

    it('should allow funding and withdraw by escrow contract from a treasury contract', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const treasury = await Treasury.deployed()

        // Initial balance
        paymentToken.transfer(treasury.address, '100000000', { from: PAYER })

        const payeeSignature = await GreetEscrow.eip712.signContractTerms(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            CONTRACT_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '100000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 1, CONTRACT_CID, '100000000', payeeSignature, '0x00', { from: PAYER })

        const balance1 = await paymentToken.balanceOf(Treasury.address)
        assert.equal(balance1.toString(), '200000000', 'invalid treasury balance')
        const escrowBalance1 = await treasury.balances(EscrowForAragon.address, paymentToken.address)
        assert.equal(escrowBalance1.toString(), '100000000', 'invalid escrow treasury balance')
        
        await escrow.releaseMilestone(CONTRACT_CID, 1, '50000000', { from: PAYER })
        await escrow.withdrawMilestone(CONTRACT_CID, 1, { from: PAYER })

        const balance2 = await paymentToken.balanceOf(Treasury.address)
        assert.equal(balance2.toString(), '150000000', 'invalid treasury balance')
        const escrowBalance2 = await treasury.balances(EscrowForAragon.address, paymentToken.address)
        assert.equal(escrowBalance2.toString(), '50000000', 'invalid escrow treasury balance')

        await escrow.releaseMilestone(CONTRACT_CID, 1, '50000000', { from: PAYER })
        await escrow.withdrawMilestone(CONTRACT_CID, 1, { from: PAYER })

        const balance3 = await paymentToken.balanceOf(Treasury.address)
        assert.equal(balance3.toString(), '100000000', 'invalid treasury balance')
        const escrowBalance3 = await treasury.balances(EscrowForAragon.address, paymentToken.address)
        assert.equal(escrowBalance3.toString(), '0', 'invalid escrow treasury balance')
    })

    it('should allow to execute scripts', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.registerMilestone.sendTransaction(
            CONTRACT_CID,
            2,
            PaymentToken.address,
            EscrowForAragon.address,
            PAYEE,
            PAYER,
            EscrowDisputeManager.address,
            0,
            '200000000',
            CONTRACT_CID, { from: PAYER }
        )

        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '200000000', { from: PAYER })
        const operation = new utils.Interface([
            "function fundMilestone(bytes32 _cid, uint16 _index, uint256 _amountToFund)"
        ])
            .encodeFunctionData("fundMilestone", [CONTRACT_CID, 2, '200000000'])
        await escrow.multicall.sendTransaction([operation], { from: PAYER })

        const milestone2 = await escrow.milestones.call(genMid(CONTRACT_CID, 2), { from: RANDOM })
        assert.equal(milestone2.fundedAmount.toString(), '200000000', "should be 200000000")
    })
})

// State persists sequencially for each test
contract('AragonCourtDisputerV1', accounts => {
    const PAYER = accounts[1]
    const PAYEE = accounts[2]
    const RANDOM = accounts[3]

    it('should allow registration of new contract', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: Treasury.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }]
        await escrow.registerContract.sendTransaction(
            CONTRACT_CID,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestone_params, { from: RANDOM }
        )

        const paymentToken = await PaymentToken.deployed()
        const insuranceManager = await InsuranceManager.deployed()
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            CONTRACT_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 1, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })

        await paymentToken.transfer(insuranceManager.address, '1000000000', { from: PAYER })
    })

    it('should allow dispute with insurance full coverage', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const insuranceManager = await InsuranceManager.deployed()

        const balance1 = await paymentToken.balanceOf(PAYER)
        const balance2 = await paymentToken.balanceOf(insuranceManager.address)
        await insuranceManager.registerCoverage.sendTransaction(CONTRACT_CID, paymentToken.address, '100000000', { from: accounts[0] })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 1, 100, 0, STATEMENT_CID, { from: PAYER })
        await sleep(2000) // should pass at least 1 second from first suggestion
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 1, false, { from: PAYER })
        const balance3 = await paymentToken.balanceOf(PAYER)
        const balance4 = await paymentToken.balanceOf(insuranceManager.address)
        assert.equal(balance1.eq(balance3), true, "insurance not used")
        assert.equal(balance2.sub(balance4).toString(), '100000000', "should be 100")
    })
})

// State persists sequencially for each test
contract('AragonCourtDisputerV1', accounts => {
    const PAYER = accounts[1]
    const PAYEE = accounts[2]
    const RANDOM = accounts[3]

    it('should allow registration of new contract', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: Treasury.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: Treasury.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }]
        await escrow.registerContract.sendTransaction(
            CONTRACT_CID,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestone_params, { from: RANDOM }
        )

        const paymentToken = await PaymentToken.deployed()
        const insuranceManager = await InsuranceManager.deployed()
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            CONTRACT_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 1, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 2, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })

        await paymentToken.transfer(insuranceManager.address, '1000000000', { from: PAYER })
    })

    it('should allow dispute with insurance partial coverage', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const insuranceManager = await InsuranceManager.deployed()

        const balance1 = await paymentToken.balanceOf(PAYER)
        const balance2 = await paymentToken.balanceOf(insuranceManager.address)
        await insuranceManager.registerCoverage.sendTransaction(CONTRACT_CID, paymentToken.address, '50000000', { from: accounts[0] })
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '50000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 1, 100, 0, STATEMENT_CID, { from: PAYER })
        await sleep(2000) // should pass at least 1 second from first suggestion
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 1, false, { from: PAYER })
        const balance3 = await paymentToken.balanceOf(PAYER)
        const balance4 = await paymentToken.balanceOf(insuranceManager.address)
        assert.equal(balance1.sub(balance3).toString(), '50000000', "should be 50")
        assert.equal(balance2.sub(balance4).toString(), '50000000', "should be 50")
    })

    it('should allow dispute with insurance no coverage', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const insuranceManager = await InsuranceManager.deployed()

        const balance1 = await paymentToken.balanceOf(PAYER)
        const balance2 = await paymentToken.balanceOf(insuranceManager.address)
        await insuranceManager.registerCoverage.sendTransaction(CONTRACT_CID, paymentToken.address, '0', { from: accounts[0] })
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 2, 0, 100, STATEMENT_CID, { from: PAYER })
        await sleep(2000) // should pass at least 1 second from first suggestion
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 2, false, { from: PAYER })
        const balance3 = await paymentToken.balanceOf(PAYER)
        const balance4 = await paymentToken.balanceOf(insuranceManager.address)
        assert.equal(balance1.sub(balance3).toString(), '100000000', "should be 100")
        assert.equal(balance2.eq(balance4), true, "used insurance without coverage")
    })
})

// State persists sequencially for each test
contract('Registry', accounts => {
    const PAYER = accounts[1]
    const RANDOM = accounts[3]

    it('should allow to change insurance manager', async() => {
        const registry = await Registry.deployed()
        await registry.setInsuranceManager.sendTransaction(ZERO_ADDRESS, { from: accounts[0] })
        assert.equal(await registry.insuranceManager(), ZERO_ADDRESS, "not changed insurance manager")
    })

    it('should not allow to change insurance manager by non-owner', async() => {
        const registry = await Registry.deployed()
        await truffleAssert.fails(
            registry.setInsuranceManager.sendTransaction(PAYER, { from: RANDOM }),
            null,
            "Ownable: caller is not the owner"
        );
    })
})
const { assert } = require('chai')
const truffleAssert = require('truffle-assertions')
const { GreetEscrow } = require('../js/dist/server')

const EscrowDisputeManager = artifacts.require('AragonCourtDisputerV1')
const EscrowForAragon = artifacts.require('EscrowV1')
const PaymentToken = artifacts.require('PaymentTokenMock')
const AragonProtocol = artifacts.require('AragonProtocolMock')

const RULE_NO_VOTE = '2';
const RULE_PAYER_WON = '4';

const CONTRACT_CID = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a010b6' //'Qmb3u8PnDeU3stUeo68d6HXhjpmBEU71tJwsBeFreDezfo'
const STATEMENT_CID = '0xcf116662909ada21c1ce86fce1b5d388e2edcb08856004fe53c2d17632b85d73' //'QmcGxVTu2BDV5bU9AeLxhTMS6aXDWSL5dN9HDSxNgV5T2J'

const genMid = GreetEscrow.util.genMid

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// State persists sequencially for each test
contract('EscrowV1', accounts => {
    const PAYER = accounts[1]
    const PAYEE = accounts[2]
    const RANDOM = accounts[3]
    const PAYER_DEL = accounts[4]
    const PAYEE_DEL = accounts[5]

    it('should allow registration of new contract', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: Math.floor(new Date().getTime() / 1000) - 1, // Reached the maturity date
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: Math.floor(new Date().getTime() / 1000) + 60, // Not reached the maturity date
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: Math.floor(new Date().getTime() / 1000) -1, // Reached the maturity date
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: Math.floor(new Date().getTime() / 1000) + 60, // Not reached the maturity date
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: Math.floor(new Date().getTime() / 1000) + 60, // Not reached the maturity date
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: Math.floor(new Date().getTime() / 1000) + 60, // Not reached the maturity date
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: Math.floor(new Date().getTime() / 1000) + 60, // Not reached the maturity date
            amount: '100000000',
            parentIndex: 0
        }]
        await escrow.registerContract.sendTransaction(
            CONTRACT_CID,
            PAYER,
            PAYER_DEL,
            PAYEE,
            PAYEE_DEL,
            milestone_params, { from: RANDOM }
        )

        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            CONTRACT_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 1, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '50000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 2, CONTRACT_CID, '50000000', payeeSignature, '0x00', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '100000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 3, CONTRACT_CID, '100000000', payeeSignature, '0x00', { from: PAYER })
    })

    it('should allow withdrawal if maturity by delivery deadline has been reached', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const balance1 = await paymentToken.balanceOf.call(PAYEE)
        escrow.withdrawMilestone.sendTransaction(CONTRACT_CID, 1, { from: PAYEE })
        const balance2 = await paymentToken.balanceOf.call(PAYEE)
        assert.equal(balance2.sub(balance1).toString(), '50000000', "Invalid earnings")
    })

    it('should not allow withdrawal if maturity was not reached', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
          escrow.withdrawMilestone.sendTransaction(CONTRACT_CID, 2, { from: PAYEE }),
            null,
            "Nothing to withdraw"
        );
    })

    it('should not allow withdrawal if milestone is in dispute', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 3, 100, 0, STATEMENT_CID, { from: PAYER })
        await sleep(2000) // should pass at least 1 second from first suggestion
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 3, true, { from: PAYER })

        await truffleAssert.fails(
          escrow.withdrawMilestone.sendTransaction(CONTRACT_CID, 3, { from: PAYEE }),
            null,
            "Nothing to withdraw"
        );
    })

    it('should not allow withdrawal if milestone dispute has failed and was restarted', async() => {
        const escrow = await EscrowForAragon.deployed()
        const protocol = await AragonProtocol.deployed()
        await protocol.setRulingForDispute(1, RULE_NO_VOTE, { from: RANDOM }) // dispute indexation starts from 1
        await escrow.executeSettlement(CONTRACT_CID, 3, { from: RANDOM })

        await truffleAssert.fails(
          escrow.withdrawMilestone.sendTransaction(CONTRACT_CID, 3, { from: PAYEE }),
            null,
            "Nothing to withdraw"
        );
    })

    it('should allow refund for matured by date milestone if dispute was won by Payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        const protocol = await AragonProtocol.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowDisputeManager.address, '100000000', { from: PAYER })
        await escrow.proposeSettlement.sendTransaction(CONTRACT_CID, 3, 100, 0, STATEMENT_CID, { from: PAYER })
        await sleep(2000) // should pass at least 1 second from first suggestion
        await escrow.disputeSettlement.sendTransaction(CONTRACT_CID, 3, true, { from: PAYER })
        await protocol.setRulingForDispute(2, RULE_PAYER_WON, { from: RANDOM })
        
        const balance1 = await paymentToken.balanceOf.call(PAYER)
        await escrow.executeSettlement(CONTRACT_CID, 3, { from: RANDOM })
        await escrow.refundMilestone(CONTRACT_CID, 3, { from: RANDOM })
        const balance2 = await paymentToken.balanceOf.call(PAYER)
        assert.equal(balance2.sub(balance1).toString(), '100000000', "Invalid refund")
    })

    it('should not allow to cancel auto-release by random party', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.stopMilestoneAutoRelease.sendTransaction(CONTRACT_CID, 4, { from: RANDOM }),
            null,
            "Not a disputer"
        );
    })

    it('should allow to cancel auto-release by payer delegate', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.stopMilestoneAutoRelease.sendTransaction(CONTRACT_CID, 4, { from: PAYER_DEL })
        const milestone4 = await escrow.milestones.call(genMid(CONTRACT_CID, 4), { from: RANDOM })
        assert.equal(milestone4.autoReleasedAt.toString(), '0', "Should be 0")
    })

    it('should allow to cancel auto-release by payee delegate', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.stopMilestoneAutoRelease.sendTransaction(CONTRACT_CID, 5, { from: PAYEE_DEL })
        const milestone5 = await escrow.milestones.call(genMid(CONTRACT_CID, 5), { from: RANDOM })
        assert.equal(milestone5.autoReleasedAt.toString(), '0', "Should be 0")
    })

    it('should allow to cancel auto-release by payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.stopMilestoneAutoRelease.sendTransaction(CONTRACT_CID, 6, { from: PAYER })
        const milestone6 = await escrow.milestones.call(genMid(CONTRACT_CID, 6), { from: RANDOM })
        assert.equal(milestone6.autoReleasedAt.toString(), '0', "Should be 0")
    })

    it('should allow to cancel auto-release by payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.stopMilestoneAutoRelease.sendTransaction(CONTRACT_CID, 7, { from: PAYEE })
        const milestone7 = await escrow.milestones.call(genMid(CONTRACT_CID, 7), { from: RANDOM })
        assert.equal(milestone7.autoReleasedAt.toString(), '0', "Should be 0")
    })
})
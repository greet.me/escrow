const { assert } = require('chai')
const truffleAssert = require('truffle-assertions')
const { GreetEscrow } = require('../js/dist/server')

const EscrowDisputeManager = artifacts.require('AragonCourtDisputerV1')
const EscrowForAragon = artifacts.require('EscrowV1')
const PaymentToken = artifacts.require('PaymentTokenMock')

const ZERO_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

const CONTRACT_CID = '0xbcdd5c0f6298bb7bc8f9c3f7888d641f4ed56bc64003f5376cbb5069c6a010b6' //'Qmb3u8PnDeU3stUeo68d6HXhjpmBEU71tJwsBeFreDezfo'
const AMENDMENT1_CID = '0xcf116662909ada21c1ce86fce1b5d388e2edcb08856004fe53c2d17632b85d73' //'QmcGxVTu2BDV5bU9AeLxhTMS6aXDWSL5dN9HDSxNgV5T2J'

function ether(value) {
    return web3.utils.toWei(value, 'ether')
}

const genMid = GreetEscrow.util.genMid
const genAuid = GreetEscrow.util.genAuid

// State persists sequencially for each test
contract('EscrowV1', accounts => {
    const PAYER = accounts[1]
    const PAYEE = accounts[2]
    const RANDOM = accounts[3]
    const DEL1 = accounts[4]
    const DEL2 = accounts[6]


    // CONTRACT

    it('should not allow parties or delegates to be the same address', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }]

        await truffleAssert.fails(
            escrow.registerContract.sendTransaction(
                CONTRACT_CID,
                PAYER,
                PAYEE,
                PAYER,
                ZERO_ADDRESS,
                milestone_params, { from: RANDOM }
            ),
            null,
            "Invalid parties"
        );

        await truffleAssert.fails(
            escrow.registerContract.sendTransaction(
                CONTRACT_CID,
                PAYER,
                PAYEE,
                PAYEE,
                PAYEE,
                milestone_params, { from: RANDOM }
            ),
            null,
            "Invalid parties"
        );
    })

    it('should allow registration of new contract', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }, {
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '150000000',
            parentIndex: 0
        }]
        await escrow.registerContract.sendTransaction(
            CONTRACT_CID,
            PAYER,
            PAYER,
            PAYEE,
            PAYEE,
            milestone_params, { from: RANDOM }
        )
        const contract = await escrow.contracts.call(CONTRACT_CID, { from: RANDOM })
        assert.equal(contract.payer, PAYER, "not payer")
        assert.equal(contract.payerDelegate, PAYER, "not payer")
        assert.equal(contract.payee, PAYEE, "not payee")
        assert.equal(contract.payeeDelegate, PAYEE, "not payee")

        const milestone1 = await escrow.milestones.call(genMid(CONTRACT_CID, 1), { from: RANDOM })
        assert.equal(milestone1.paymentToken, PaymentToken.address, "invalid payment token")
        assert.equal(milestone1.treasury, EscrowForAragon.address, "invalid treasury address")
        assert.equal(milestone1.payeeAccount, PAYEE, "invalid PAYEE address")
        assert.equal(milestone1.refundAccount, PAYER, "invalid PAYER address")
        assert.equal(milestone1.escrowDisputeManager, EscrowDisputeManager.address, "invalid EscrowDisputeManager address")
        assert.equal(milestone1.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone1.fundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone1.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone1.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone1.claimedAmount.toString(), '0', "should be 0")

        const milestone2 = await escrow.milestones.call(genMid(CONTRACT_CID, 2), { from: RANDOM })
        assert.equal(milestone2.paymentToken, PaymentToken.address, "invalid payment token")
        assert.equal(milestone2.treasury, EscrowForAragon.address, "invalid treasury address")
        assert.equal(milestone2.payeeAccount, PAYEE, "invalid PAYEE address")
        assert.equal(milestone2.refundAccount, PAYER, "invalid PAYER address")
        assert.equal(milestone2.escrowDisputeManager, EscrowDisputeManager.address, "invalid EscrowDisputeManager address")
        assert.equal(milestone2.amount.toString(), '150000000', "invalid sum")
        assert.equal(milestone2.fundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone2.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone2.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone2.claimedAmount.toString(), '0', "should be 0")
    })

    it('should not allow duplicate contracts', async() => {
        const escrow = await EscrowForAragon.deployed()
        const milestone_params = [{
            paymentToken: PaymentToken.address,
            treasury: EscrowForAragon.address,
            payeeAccount: PAYEE,
            refundAccount: PAYER,
            escrowDisputeManager: EscrowDisputeManager.address,
            autoReleasedAt: 0,
            amount: '100000000',
            parentIndex: 0
        }]

        await truffleAssert.fails(
            escrow.registerContract.sendTransaction(
                CONTRACT_CID,
                PAYER,
                PAYER,
                PAYEE,
                PAYEE,
                milestone_params, { from: RANDOM }
            ),
            null,
            "Contract exists"
        );
    })

    // MILESTONES

    it('should register additional milestone without amendment', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.registerMilestone.sendTransaction(
            CONTRACT_CID,
            3,
            PaymentToken.address,
            EscrowForAragon.address,
            PAYEE,
            PAYER,
            EscrowDisputeManager.address,
            0,
            '200000000',
            CONTRACT_CID, { from: PAYER }
        )

        const milestone3 = await escrow.milestones.call(genMid(CONTRACT_CID, 3), { from: RANDOM })
        assert.equal(milestone3.paymentToken, PaymentToken.address, "invalid payment token")
        assert.equal(milestone3.treasury, EscrowForAragon.address, "invalid treasury address")
        assert.equal(milestone3.payeeAccount, PAYEE, "invalid PAYEE address")
        assert.equal(milestone3.refundAccount, PAYER, "invalid PAYER address")
        assert.equal(milestone3.escrowDisputeManager, EscrowDisputeManager.address, "invalid EscrowDisputeManager address")
        assert.equal(milestone3.amount.toString(), '200000000', "invalid sum")
        assert.equal(milestone3.fundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone3.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone3.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone3.claimedAmount.toString(), '0', "should be 0")

        const amendment = await escrow.contractVersionProposals.call(genAuid(CONTRACT_CID, CONTRACT_CID))
        assert.equal(amendment.termsCid, ZERO_BYTES32, "not empty")
    })

    it('should register additional milestone with amendment', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.registerMilestone.sendTransaction(
            CONTRACT_CID,
            4,
            PaymentToken.address,
            EscrowForAragon.address,
            PAYEE,
            PAYER,
            EscrowDisputeManager.address,
            0,
            '200000000',
            AMENDMENT1_CID, { from: PAYER }
        )

        const milestone4 = await escrow.milestones.call(genMid(CONTRACT_CID, 4), { from: RANDOM })
        assert.equal(milestone4.paymentToken, PaymentToken.address, "invalid payment token")
        assert.equal(milestone4.treasury, EscrowForAragon.address, "invalid treasury address")
        assert.equal(milestone4.payeeAccount, PAYEE, "invalid PAYEE address")
        assert.equal(milestone4.refundAccount, PAYER, "invalid PAYER address")
        assert.equal(milestone4.escrowDisputeManager, EscrowDisputeManager.address, "invalid EscrowDisputeManager address")
        assert.equal(milestone4.amount.toString(), '200000000', "invalid sum")
        assert.equal(milestone4.fundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone4.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone4.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone4.claimedAmount.toString(), '0', "should be 0")

        const amendment = await escrow.contractVersionProposals.call(genAuid(CONTRACT_CID, AMENDMENT1_CID))
        assert.equal(amendment.termsCid, AMENDMENT1_CID, "is empty")
    })

    it('should register additional child milestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.registerMilestone.sendTransaction(
            CONTRACT_CID,
            401,
            PaymentToken.address,
            EscrowForAragon.address,
            PAYEE,
            PAYER,
            EscrowDisputeManager.address,
            0,
            '200000000',
            CONTRACT_CID, { from: PAYER }
        )

        const milestone401 = await escrow.milestones.call(genMid(CONTRACT_CID, 401), { from: RANDOM })
        assert.equal(milestone401.paymentToken, PaymentToken.address, "invalid payment token")
        assert.equal(milestone401.treasury, EscrowForAragon.address, "invalid treasury address")
        assert.equal(milestone401.payeeAccount, PAYEE, "invalid PAYEE address")
        assert.equal(milestone401.refundAccount, PAYER, "invalid PAYER address")
        assert.equal(milestone401.escrowDisputeManager, EscrowDisputeManager.address, "invalid EscrowDisputeManager address")
        assert.equal(milestone401.amount.toString(), '200000000', "invalid sum")
        assert.equal(milestone401.fundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone401.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone401.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone401.claimedAmount.toString(), '0', "should be 0")
    })

    it('should not allow duplicate milestones', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.registerMilestone.sendTransaction(
                CONTRACT_CID,
                1,
                PaymentToken.address,
                EscrowForAragon.address,
                PAYEE,
                PAYER,
                EscrowDisputeManager.address,
                0,
                '200000000',
                AMENDMENT1_CID, { from: PAYER }
            ),
            null,
            "Milestone exists"
        );
    })

    it('should not allow milestones from non-party address', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.registerMilestone.sendTransaction(
                CONTRACT_CID,
                5,
                PaymentToken.address,
                EscrowForAragon.address,
                PAYEE,
                PAYER,
                EscrowDisputeManager.address,
                0,
                '200000000',
                AMENDMENT1_CID, { from: RANDOM }
            ),
            null,
            "Not a party"
        );
    })

    // FUNDING

    it('should not allow funding of non-approved milestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.fundMilestone.sendTransaction(
                CONTRACT_CID,
                1,
                '100000000', { from: RANDOM }
            ),
            null,
            "Not signed"
        );
    })

    it('should allow funding of approved milestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await escrow.signAndProposeContractVersion.sendTransaction(CONTRACT_CID, CONTRACT_CID, { from: PAYER })
        await escrow.signAndApproveContractVersion.sendTransaction(CONTRACT_CID, CONTRACT_CID, { from: PAYEE })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '100000000', { from: PAYER })
        await escrow.fundMilestone.sendTransaction(CONTRACT_CID, 1, '100000000', { from: PAYER })
        const milestone1 = await escrow.milestones.call(genMid(CONTRACT_CID, 1), { from: RANDOM })
        assert.equal(milestone1.fundedAmount.toString(), '100000000', "not funded")
    })

    it('should not allow over-funding of approved milestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '100000000', { from: PAYER })
        await truffleAssert.fails(
            escrow.fundMilestone.sendTransaction(CONTRACT_CID, 1, '100000000', { from: PAYER }),
            null,
            "Funding not needed"
        )
    })

    it('should fail funding of approved milestone if there is not enough funds', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '150000000', { from: RANDOM })
        await truffleAssert.fails(
            escrow.fundMilestone.sendTransaction(CONTRACT_CID, 2, '150000000', { from: RANDOM }),
            null,
            "ERC20: transfer amount exceeds balance"
        )
    })

    it('should allow partial funding (by 3rd party) of approved milestone', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        await paymentToken.transfer(RANDOM, '50000000', { from: PAYER })
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '150000000', { from: RANDOM })
        await escrow.fundMilestone.sendTransaction(CONTRACT_CID, 2, '50000000', { from: RANDOM })
        const milestone2 = await escrow.milestones.call(genMid(CONTRACT_CID, 2), { from: RANDOM })
        assert.equal(milestone2.fundedAmount.toString(), '50000000', "not funded")
    })

    it('should allow funding with payee signature', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signContractTerms(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            CONTRACT_CID,
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '200000000', { from: PAYER })
        await escrow.signAndFundMilestone(CONTRACT_CID, 3, CONTRACT_CID, '200000000', payeeSignature, '0x00', { from: PAYER })
        const milestone3 = await escrow.milestones.call(genMid(CONTRACT_CID, 3), { from: RANDOM })
        assert.equal(milestone3.fundedAmount.toString(), '200000000', "not funded")

        await paymentToken.approve.sendTransaction(EscrowForAragon.address, '400000000', { from: PAYER })
        await escrow.fundMilestone.sendTransaction(CONTRACT_CID, 4, '200000000', { from: PAYER })
        await escrow.fundMilestone.sendTransaction(CONTRACT_CID, 401, '200000000', { from: PAYER })
        const milestone4 = await escrow.milestones.call(genMid(CONTRACT_CID, 4), { from: RANDOM })
        assert.equal(milestone4.fundedAmount.toString(), '200000000', "not funded")
        const milestone401 = await escrow.milestones.call(genMid(CONTRACT_CID, 401), { from: RANDOM })
        assert.equal(milestone401.fundedAmount.toString(), '200000000', "not funded")
    })


    // RELEASE

    it('should not allow withdrawal with a wrong amount with release signature from payer delegate', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payerSignature = await GreetEscrow.eip712.signMilestoneRelease(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            1,
            '50000000',
            (message) => web3.eth.sign(message.toString(), PAYER)
        )
        await truffleAssert.fails(
            escrow.withdrawPreApprovedMilestone.sendTransaction(CONTRACT_CID, 1, '100000000', payerSignature, { from: RANDOM }),
            null,
            "Invalid signature"
        )
    })

    it('should not allow withdrawal with release signature for another milestone from payer delegate', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payerSignature = await GreetEscrow.eip712.signMilestoneRelease(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            2,
            '100000000',
            (message) => web3.eth.sign(message.toString(), PAYER)
        )
        await truffleAssert.fails(
            escrow.withdrawPreApprovedMilestone.sendTransaction(CONTRACT_CID, 1, '100000000', payerSignature, { from: RANDOM }),
            null,
            "Invalid signature"
        )
    })

    it('should not allow withdrawal with release signature from another party', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payerSignature = await GreetEscrow.eip712.signMilestoneRelease(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            1,
            '100000000',
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await truffleAssert.fails(
            escrow.withdrawPreApprovedMilestone.sendTransaction(CONTRACT_CID, 1, '100000000', payerSignature, { from: RANDOM }),
            null,
            "Invalid signature"
        )
    })

    it('should not allow withdrawal over the funding with release signature from payer delegate', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payerSignature = await GreetEscrow.eip712.signMilestoneRelease(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            1,
            '100000001',
            (message) => web3.eth.sign(message.toString(), PAYER)
        )
        await truffleAssert.fails(
            escrow.withdrawPreApprovedMilestone.sendTransaction(CONTRACT_CID, 1, '100000001', payerSignature, { from: RANDOM }),
            null,
            "Invalid release amount"
        )
    })

    it('should allow withdraw with proper release signature from payer delegate', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payerSignature = await GreetEscrow.eip712.signMilestoneRelease(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            1,
            '100000000',
            (message) => web3.eth.sign(message.toString(), PAYER)
        )
        const balance1 = await paymentToken.balanceOf.call(EscrowForAragon.address)
        await escrow.withdrawPreApprovedMilestone.sendTransaction(CONTRACT_CID, 1, '100000000', payerSignature, { from: RANDOM })
        const earned = await paymentToken.balanceOf.call(PAYEE)
        assert.equal(earned.toString(), '100000000', "Invalid earnings")
        const balance2 = await paymentToken.balanceOf.call(EscrowForAragon.address)
        assert.equal(balance1.sub(balance2).eq(earned), true, "Invalid spendings")

        const milestone1 = await escrow.milestones.call(genMid(CONTRACT_CID, 1), { from: RANDOM })
        assert.equal(milestone1.amount.toString(), '100000000', "invalid sum")
        assert.equal(milestone1.fundedAmount.toString(), '100000000', "should be 100")
        assert.equal(milestone1.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone1.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone1.claimedAmount.toString(), '100000000', "should be 100")
    })

    // REFUNDS

    it('should not allow refund with a wrong amount with release signature from payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signMilestoneRefund(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            3,
            '50000000',
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await truffleAssert.fails(
            escrow.refundPreApprovedMilestone.sendTransaction(CONTRACT_CID, 3, '200000000', payeeSignature, { from: RANDOM }),
            null,
            "Invalid signature"
        )
    })

    it('should not allow refund with release signature for another milestone from payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signMilestoneRefund(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            2,
            '200000000',
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await truffleAssert.fails(
            escrow.refundPreApprovedMilestone.sendTransaction(CONTRACT_CID, 3, '200000000', payeeSignature, { from: RANDOM }),
            null,
            "Invalid signature"
        )
    })

    it('should not allow refund with release signature from another party', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signMilestoneRefund(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            3,
            '200000000',
            (message) => web3.eth.sign(message.toString(), PAYER)
        )
        await truffleAssert.fails(
            escrow.refundPreApprovedMilestone.sendTransaction(CONTRACT_CID, 3, '200000000', payeeSignature, { from: RANDOM }),
            null,
            "Invalid signature"
        )
    })

    it('should not allow refund over the funding with release signature from payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signMilestoneRefund(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            3,
            '200000001',
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        await truffleAssert.fails(
            escrow.refundPreApprovedMilestone.sendTransaction(CONTRACT_CID, 3, '200000001', payeeSignature, { from: RANDOM }),
            null,
            "Invalid release amount"
        )
    })

    it('should allow refund with proper release signature from payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        const payeeSignature = await GreetEscrow.eip712.signMilestoneRefund(
            1,
            EscrowForAragon.address,
            CONTRACT_CID,
            3,
            '200000000',
            (message) => web3.eth.sign(message.toString(), PAYEE)
        )
        const beforeRefunded = await paymentToken.balanceOf.call(PAYER)
        const balance1 = await paymentToken.balanceOf.call(EscrowForAragon.address)
        await escrow.refundPreApprovedMilestone.sendTransaction(CONTRACT_CID, 3, '200000000', payeeSignature, { from: RANDOM })
        const refunded = await paymentToken.balanceOf.call(PAYER).then((v) => v.sub(beforeRefunded))
        assert.equal(refunded.toString(), '200000000', "Invalid earnings")
        const balance2 = await paymentToken.balanceOf.call(EscrowForAragon.address)
        assert.equal(balance1.sub(balance2).eq(refunded), true, "Invalid spendings")

        const milestone3 = await escrow.milestones.call(genMid(CONTRACT_CID, 3), { from: RANDOM })
        assert.equal(milestone3.amount.toString(), '200000000', "invalid sum")
        assert.equal(milestone3.fundedAmount.toString(), '200000000', "should be 200")
        assert.equal(milestone3.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone3.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone3.claimedAmount.toString(), '200000000', "should be 200")
    })

    it('should allow onchain cancelation from payee and refund claim from payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        const paymentToken = await PaymentToken.deployed()
        
        await escrow.cancelMilestone.sendTransaction(CONTRACT_CID, 4, '100000000', { from: PAYEE })
        const beforeRefunded = await paymentToken.balanceOf.call(PAYER)
        const balance1 = await paymentToken.balanceOf.call(EscrowForAragon.address)
        await escrow.refundMilestone.sendTransaction(CONTRACT_CID, 4, { from: PAYEE })
        const refunded = await paymentToken.balanceOf.call(PAYER).then((v) => v.sub(beforeRefunded))
        assert.equal(refunded.toString(), '100000000', "Invalid earnings")
        const balance2 = await paymentToken.balanceOf.call(EscrowForAragon.address)
        assert.equal(balance1.sub(balance2).eq(refunded), true, "Invalid spendings")

        const milestone4 = await escrow.milestones.call(genMid(CONTRACT_CID, 4), { from: RANDOM })
        assert.equal(milestone4.amount.toString(), '200000000', "invalid sum")
        assert.equal(milestone4.fundedAmount.toString(), '200000000', "should be 200")
        assert.equal(milestone4.refundedAmount.toString(), '0', "should be 0")
        assert.equal(milestone4.releasedAmount.toString(), '0', "should be 0")
        assert.equal(milestone4.claimedAmount.toString(), '100000000', "should be 100")
    })

    
    // CHANGE DELEGATES

    it('should not allow to change delegate by random', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.changeDelegate(CONTRACT_CID, DEL2, { from: RANDOM }),
            null,
            ""
        )
    })

    it('should not allow to change delegate to empty address by payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.changeDelegate(CONTRACT_CID, ZERO_ADDRESS, { from: PAYER }),
            null,
            "Invalid delegate"
        )
    })

    it('should not allow to change delegate to empty address by payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        await truffleAssert.fails(
            escrow.changeDelegate(CONTRACT_CID, ZERO_ADDRESS, { from: PAYEE }),
            null,
            "Invalid delegate"
        )
    })

    it('should allow to change delegate by payer', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.changeDelegate(CONTRACT_CID, DEL1, { from: PAYER })
        const contract = await escrow.contracts.call(CONTRACT_CID, { from: RANDOM })
        assert.equal(contract.payerDelegate, DEL1, "not a proper delegate")
    })

    it('should allow to change delegate by payee', async() => {
        const escrow = await EscrowForAragon.deployed()
        await escrow.changeDelegate(CONTRACT_CID, DEL2, { from: PAYEE })
        const contract = await escrow.contracts.call(CONTRACT_CID, { from: RANDOM })
        assert.equal(contract.payeeDelegate, DEL2, "not a proper delegate")
    })
})
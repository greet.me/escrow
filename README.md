# Escrow
Solidity smart contracts and front-end API to access greet escrow solution.

# Javascript API

Javascript library with methods used to interact with escrow smart contracts.

API is exposed through `escrowV1` module e.g. `import { GreetEscrow } from "greet_escrow"`,
then `GreetEscrow.escrowV1.registerContract(...)`

If not described otherwise, expect promise as return object.
All on-chain calls return blockchain transaction object.
All off-chain calls return string with signature in a hex form.
Views responses for on-chain calls are documented.

## Custom types

### MilestoneParams

    {
        paymentToken: string(42),
        treasury: string(42),
        payeeAccount: string(42),
        refundAccount: string(42),
        autoReleasedAt: uint256,
        amount: uint256,
        parentIndex: uint16
    }    

## registerContract (on-chain)

Registers contract data on-chain:

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * string(42) __payer__ - Party which pays for the contract or on behalf of which the funding was done.
    * string(42) __payee__ - Party which recieves the payment.
    * string(42) __payerDelegate__ - (optional) Delegate who can release or dispute contract on behalf of payer.
    * string(42) __payeeDelegate__ - (optional) Delegate who can release or dispute contract on behalf of payee.
    * [MilestoneParams] __milestones__ - (optional) Delivery amounts and payment tokens.

### Example

```
GreetEscrow.escrowV1.registerContract(
    5777,
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
    '0x69311aebe311B5cA7aFdA47801388b06A4dF7B69',
    '0x69311aebe311B5cA7aFdA47801388b06A4dF7B69',
    '0x4253e41227F7af597176963A4F1e8399539e60D5',
    '0x4253e41227F7af597176963A4F1e8399539e60D5',
    []
)
```

## registerMilestone (on-chain)

Adds milestone to existing contract, optionally with proposal to amend existing terms:

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index, starts from 1. Indexes greater than 99 are reserved for child milestones, disputes for which are resolved automatically as their parent. Child index formula: PARENT_INDEX * 100 + 1..99 e.g. the child milestone for milestone 2 will have index 201
    * string(42) __paymentToken__ - ERC20 token address used for payment in milestone
    * string(42) __treasury__ - Address where funds will be stored in escrow, use escrow contract address if in doubt
    * string(42) __payeeAccount__ - Address where payment should be delivered, mainly useful for vesting contracts
    * string(42) __refundAccount__ - Address where payment should be refunded, mainly useful in case of separate treasury address or sponsorships
    * uint256 __autoReleasedAt__ - Unix timestamp for milestone deadline, pass 0 if none
    * uint256 __amount__ - Amount of ERC20 token to be used as payment, can be different denominations: 6 (USDC, USDT), 18 (DAI, other ERC20 token)
    * string(46) __amendmentCid__ - (optional) Propose new contract terms, pass address(0) or current terms if not needed

### Example

```
GreetEscrow.escrowV1.registerMilestone(
    5777,
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
    1,
    '0xee0d6173f4DcF090C569fB6787Dba39D8128785d',
    '0xcCAE202837db5128452bDaBc726aBAe91227C472',
    0,
    '100000000',
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s'
)
```

## isApprovedContractVersion (on-chain, view)

Check if specific contract version was signed by both parties.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * string(46) __termsCid__ - (optional) Terms to check, usually the same as cid, but can be different in case of amendements
 
Returns:

    * boolean - true if termsCid was approved by both parties

## getMilestone (on-chain, view)

Get milestone on-chain data.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index, starts from 1

Returns:

    * string(42) __paymentToken__ - ERC20 address for payment token
    * string(42) __treasury__ - Address where escrow funds are kept
    * string(42) __payeeAccount__ - Address where escrow funds should be paid to Payee
    * string(42) __refundAccount__ - Address where escrow funds will be refunded
    * string(42) __escrowDisputeManager__ - Address of dispute manager smart contract for this milestone
    * uint256 - __autoReleasedAt__ - Unix timestamp for deadline date, 0 if there is no formal deadline
    * uint256 - __amount__ - Total agreed amount of ERC20 to be paid in scope of milestone
    * uint256 - __fundedAmount__ - Amount of ERC20 which was actually funded by Payer
    * uint256 - __refundedAmount__ - Amount of ERC20 available for refund to Payer
    * uint256 - __releasedAmount__ - Amount of ERC20 available for withdraw to Payee
    * uint256 - __claimedAmount__ - Total amount of refunded and withdrawn ERC20 in scope of this milestone
    * uint8 - __revision__ - Current version of mileston digital terms in scope of settlement process, 0 by default


## signAndApproveContractVersion (on-chain)

Sign contract terms on-chain, usually only used when Payee is a DAO.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * string(46) __termsCid__ - (optional) (optional) Terms to sign, usually the same as cid, but can be different in case of amendements


## signMilestoneTermsForFunding (off-chain)

Sign milestone terms, allowing payer to fund the milestone.

Returned signature will be used as crypto proof that payee agreed with proposed terms.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * string(46) __milestoneTermsCid__ - (optional) Milestone terms, usually the same as cid, but can be different in case of amendements

### Example

```
GreetEscrow.escrowV1.signMilestoneTermsForFunding(
    5777,
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s'
).then(console.log)
```

## signAndFundMilestone (on-chain)

Sign terms as payer and fund the milestone. Make sure that before calling this function payer has __approved__ payment token spending for escrow smart contract.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index, starts from 1
    * string(46) __milestoneTermsCid__ - Milestone terms, usually the same as cid, but can be different in case of amendements
    * uint256 __amount__ - Amount of ERC20 token to be funded
    * string(194) __payeeSignature__ - Crypto proof collected from payee by calling `signMilestoneTermsForFunding` for that milestone
    * string(194) __payerSignature__ - (optional) Crypto proof collected from payer by calling `signMilestoneTermsForFunding` for that milestone. It's only needed if this method is called by 3rd party on behalf of payer (e.g. sponsor), pass anything (e.g. payeeSignature) in all other cases.

### Example

```
GreetEscrow.escrowV1.approveERC20ForEscrow(5777, '0xee0d6173f4DcF090C569fB6787Dba39D8128785d', '100000000')
    .then((result) => ethersProvider().waitForTransaction(result.hash)
    .then(() => {
        return GreetEscrow.escrowV1.signAndFundMilestone(
            5777,
            'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
            1,
            'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
            '100000000',
            '0x000000000000000000000000000000000000000000000000000000000000001c1e622e0f1aa5238b564f7f45f9a0e0419f3eba73921d47dc2cc2bb2cc37963ea42f285d91f09452babee4345b3d1b79673bff0daee1bf6d3b96b719c431f2e6a',
            '0x000000000000000000000000000000000000000000000000000000000000001c1e622e0f1aa5238b564f7f45f9a0e0419f3eba73921d47dc2cc2bb2cc37963ea42f285d91f09452babee4345b3d1b79673bff0daee1bf6d3b96b719c431f2e6a'
        )
    })
```

## signAndFundMilestone (on-chain)

Fund the milestone. Make sure that before calling this function payer has __approved__ payment token spending for escrow smart contract.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index, starts from 1
    * uint256 __amount__ - Amount of ERC20 token to be funded

### Example

```
GreetEscrow.escrowV1.approveERC20ForEscrow(5777, '0xee0d6173f4DcF090C569fB6787Dba39D8128785d', '100000000')
    .then((result) => ethersProvider().waitForTransaction(result.hash)
    .then(() => {
        return GreetEscrow.escrowV1.fundMilestone(
            5777,
            'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
            1,
            '100000000'
        )
    })
```

## signWithdrawApproval (off-chain)

Authorize payee with signature to withdraw specific amount of payment token from milestone.

Returned signature will be used as crypto proof that payer agreed to release the funds.

_Can be signed by delegate._

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index, starts from 1
    * uint256 __amount__ - Amount of ERC20 payment token authorized for withdrawal

### Example

```
GreetEscrow.escrowV1.signWithdrawApproval(
    5777,
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
    1,
    '100000000'
).then(console.log)
```

## signRefundApproval (off-chain)

Authorize payer with signature to withdraw specific amount of payment token from milestone.

Returned signature will be used as crypto proof that payee agreed to refund the funds.

_Can be signed by delegate._

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index, starts from 1
    * uint256 __amount__ - Amount of ERC20 payment token authorized for refund

### Example

```
GreetEscrow.escrowV1.signRefundApproval(
    5777,
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
    1,
    '100000000'
).then(console.log)
```

## withdrawPreApprovedMilestone (on-chain)

Withdraw authorized amount to payee account.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index, starts from 1
    * uint256 __amount__ - Amount of ERC20 payment token authorized for withdrawal
    * string(194) __payerDelegateSignature__ - Crypto proof collected from payer delegate by calling `signWithdrawApproval` for that milestone and amount

### Example

```
GreetEscrow.escrowV1.withdrawPreApprovedMilestone(
    5777,
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
    1,
    '100000000',
    '0x000000000000000000000000000000000000000000000000000000000000001b6f9e14bfd4651ad9ec0a8081091a5e115fa200b571e06c1a56a0caa403120a312b3ab33bbf3cbfd269bee099d67b6f8e55fc9683dc493f30f7824625e7f19bdf'
)
```

## refundPreApprovedMilestone (on-chain)

Refund authorized amount to payer account.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index, starts from 1
    * uint256 __amount__ - Amount of ERC20 payment token authorized for refund
    * string(194) __payeeDelegateSignature__ - Crypto proof collected from payee delegate by calling `signRefundApproval` for that milestone and amount

### Example

```
GreetEscrow.escrowV1.refundPreApprovedMilestone(
    5777,
    'QmUqB9dWDCeZ5nth9YKRJTQ6PcnfrGPPx1vzdyNWV6rh8s',
    1,
    '100000000',
    '0x000000000000000000000000000000000000000000000000000000000000001b6f9e14bfd4651ad9ec0a8081091a5e115fa200b571e06c1a56a0caa403120a312b3ab33bbf3cbfd269bee099d67b6f8e55fc9683dc493f30f7824625e7f19bdf'
)
```

## proposeSettlement (on-chain)

Propose settlement as payee, payer or delegate, suggest percent ratio of milestone funds to be released and refunded (100 in total). Attach statement as IPFS cid which will be sent if escaled to Aragon Protocol, other party will also see this statement.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index to dispute
    * uint256 __refundedPercent__ - 0-100 value of funds for refund.
    * uint256 __releasedPercent__ - 0-100 value of funds for release.
    * string(46) __statement__ - IPFS cid (v0) describing the case from your side.

## disputeSettlement (on-chain)

Escalate settlement to Aragon Protocol as payee, payer or delegate, paying the dispute fee (currently 150 DAI). Make sure that before calling this function initiator has __approved__ Aragon Protocol payment token (currently DAI) spending for escrow smart contract.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index to dispute
    * bool __ignoreCoverage__ - Pass true, while insurance coverage is not implemented to save gas

## submitEvidence (on-chain)

Payee, payer or delegate can submit additional evidences which should help Aragon Protocol Guardians to resolve the dispute.

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index in dispute
    * string __evidence__ - Arbitary string or hexlified IPFS hash to be used as evidence

## executeSettlement (on-chain)

After the dispute is resolved, the funds will be distributed according to the winner's claim.

_Can be called by anyone as ruling is static_

    * uint256 __networkId__ - Blockchain network id
    * string(46) __cid__ - Contract's IPFS cid (v0)
    * uint16 __index__ - Milestone index for which dispute was completed

# Smart contracts

## Terms

* __Payer__ - the beneficiary of delivered services or goods.
* __Payee__ - the party which is delivering services or goods for the payment.
* __PayerDelegate__ - an independent party with limited mandate from the payer to dispute (request refund) or release the funded payment to the payee.
* __PayeeAccount__ - an arbitary Ethereum address which will recieve payment after successful delivery.

## Create contract

Call `registerContract`, providing the core contract data:

    * @param _cid Contract's IPFS cid.
    * @param _payer Party which pays for the contract or on behalf of which the funding was done.
    * @param _payerDelegate Delegate who can release or dispute contract on behalf of payer.
    * @param _payee Party which recieves the payment.
    * @param _payerDelegate Delegate who can release or dispute contract on behalf of payee.
    * @param _milestones Delivery amounts and payment tokens.

Milestone params:

    * @param paymentToken Address of ERC20 token used for payment in this milestone.
    * @param treasury Address of smart contract or wallet where funds are held in escrow.
    * @param payeeAccount Address where payment should be delivered, mainly useful for vesting contracts.
    * @param refundAccount Address where payment should be refunded, mainly useful in case of separate treasury address or sponsorships.
    * @param deliveryDeadling UNIX timestamp for delivery deadline, pass 0 if none.
    * @param amount Total size of milestone in ERC20 token units.

## Collect signatures

Either collect signatures from the both parties, or only Payee if Payer is able to call `signAndFundMilestone` directly.

## Fund milestone

Make sure that escrow smart contract has ERC20 approval for the `_amountToFund` before executing `signAndFundMilestone`.

Call `signAndFundMilestone`, providing the signature from the other party collected on previous step.

    * @param _cid Contract's IPFS cid.
    * @param _index Number of milestone.
    * @param _termsCid Contract IPFS cid signed by payee.
    * @param _amountToFund Amount to fund.
    * @param _payeeSignature Signed digest of terms cid by payee.
    * @param _payerSignature Signed digest of terms cid by payer, can be bytes32(0) if caller is payer.

Alternatively, this can be called by a 3rd party if he's able to collect signature from Payer.

## Optimistic flow

### Delivery approval

Collect signatures from Payer or PayerDelegate that the work was done as agreed.

### Claim reward

Call `withdrawPreApprovedMilestone`, providing the signature from Payer or PayerDelegate.

    * @param _cid Contract's IPFS cid.
    * @param _index Number of milestone.
    * @param _amount Amount to withdraw.
    * @param _payerDelegateSignature Signed digest for release of amount.

Payment will be sent to PayerAccount.

## Refund by Payer

### Request refund

Collect signature from Payee that he agrees to provide refund without dispute.

### Claim refund

Call `refundPreApprovedMilestone`, providing the signature from Payee.

    * @param _cid Contract's IPFS cid.
    * @param _index Number of milestone.
    * @param _amount Amount to refund.
    * @param _payeeSignature Signed digest for release of amount.

Payment will be sent to Payer.

## Dispute by PayerDelegate or Payee

### Propose a disputable settlement

Call `proposeSettlement` sending the case to Aragon Court.

    * @param _cid Contract's IPFS cid.
    * @param _index Number of milestone to dispute.
    * @param _refundedPercent 0-100 value of funds for refund.
    * @param _releasedPercent 0-100 value of funds for release.
    * @param _statement IPFS cid (v0) describing the case from your side.

### Escalate dispute to Aragon Protocol

Call `disputeSettlement` sending the case to Aragon Protocol.

    * @param _cid Contract's IPFS cid.
    * @param _index Number of milestone to dispute.
    * @param _ignoreCoverage true to ignore insurance module to save gas.

### Await for resolution

Aragon Court process will take some time, potentially having the appeals which will extern the resolution further.

### Apply resolution

Call `executeSettlement` gettings the funds distributed according to the winner's claim. Alternatively, other party can call it if they won and wish to withdraw / refund the payment.

    * @param _cid Contract's IPFS cid.
    * @param _index Number of milestone to dispute.

## Off-chain resolved amendments

### Collect signature

Get signature from the other party that he agrees to amend the contract.

Alternatively you can apply amendment as a 3rd party collecting signatures from Payer and Payee.

### Apply amendment

Call `preApprovedAmendment` providing one or two signatures.

    * @param _cid Contract's IPFS cid.
    * @param _amendmentCid New version of contract's IPFS cid.
    * @param _payeeSignature Signed digest of amendment cid by payee, can be bytes(0) if payee is msg.sender.
    * @param _payerSignature Signed digest of amendment cid by payer, can be bytes(0) if payer is msg.sender.

## On-chain resolved amendments

### Propose amendment

Call `signAndProposeContractVersion` as Payer or Payee.
    
    * @param _cid Contract's IPFS cid.
    * @param _termsCid New version of contract's IPFS cid.

### Approve amendment

Call `signAndApproveContractVersion` as an opposite party to apply the changes

    * @param _cid Contract's IPFS cid.
    * @param _termsCid Suggested version of contract's IPFS cid.

# Features

* Any ERC20 compliant token can be used as payment token;
* Can have multiple payment tokens in scope of one contract (can be used for joint stablecoin and project token compensation schemes);
* Can have multiple payment tokens in milestone, disputable in a batch;
* Payments can be auto-released at maturity date if not disputed;
* Can put arbitary account for payouts in scope of milestone (lightweight support for vesting contracts, different vesting schedules for different milestones and payment tokens in scope of one contract);
* Supports mutually agreed ammendments for contract terms;
* Can assign delegate to release or dispute the funds as payer;
* Can assign delegate to refund or dispute the funds as payee;
* Partial refunds and releases (mutually agreed resolutions);
* Support for arbitary treasury address (lightweight support for yielding of escrow funds);
* Disputable per milestone for funded amount in Aragon Court by either party or their delegates;
* Either party can suggest custom distribution proportions for disputes in Aragon Court;
* Most of gas fees can be subsidized by any 3rd party (gas subsidies);
* Milestones can be funded by any 3rd party (sponshorships).

# Deployments

## Mainnet

* Migrations: `0xe75daaA4A78B4bdDe832588CAC67012A2477cceb`
* [Registry](https://etherscan.io/address/0xd30BE688C053E0A308468b1c567bdfd6A210B020): `0xd30BE688C053E0A308468b1c567bdfd6A210B020`
* [AragonCourtDisputerV1](https://etherscan.io/address/0x09ac00D8f8E913a12cAe3F81bC7F495E2747A7d6#contracts): `0x09ac00D8f8E913a12cAe3F81bC7F495E2747A7d6`
* [EscrowV1](https://etherscan.io/address/0xb7a445c48B0Af091bD5ECE6BC945C7A1dc2db314#contracts): `0xb7a445c48B0Af091bD5ECE6BC945C7A1dc2db314`

## Rinkeby

* Registry: `0x64e5Da3E9720d6F3473461119E110BE6feA38992`
* [AragonCourtDisputerV1](https://rinkeby.etherscan.io/address/0x2cc0CD8bFe52043E2e05f41c314ACB3dF05E8FAa#contracts): `0x2cc0CD8bFe52043E2e05f41c314ACB3dF05E8FAa`
* [EscrowV1](https://rinkeby.etherscan.io/address/0xB688eC8DEaA33F3403C48041260Dc71778C7Bb29#contracts): `0xB688eC8DEaA33F3403C48041260Dc71778C7Bb29`

# Testing

`npm run migrate:dev`

`npm run test`

## Current state

100 tests

### Contract: AragonCourtDisputerV1

    ✓ should allow registration of new contract (3542ms, 2813048 gas)
    ✓ should not allow dispute from non-party (2043ms, 27292 gas)
    ✓ should allow dispute from payer (3158ms, 1575698 gas)
    ✓ should not allow evidences from non-party (1279ms, 27347 gas)
    ✓ should not allow resubmittion for ongoing dispute (2552ms, 35011 gas)
    ✓ show allow resolution from anyone (1054ms, 221985 gas)
    ✓ should not allow resubmittion for resolved dispute (2129ms, 35011 gas)
    ✓ should allow dispute from payee (4173ms, 1818979 gas)
    ✓ should not allow settlement of child milestones directly (696ms, 23417 gas)
    ✓ should not allow execution of settlement of child milestones if there is no parent ruling (4202ms, 45469 gas)
    ✓ should allow settlement from payee (6113ms, 2284081 gas)
    ✓ should allow automatic settlement for child milestones by parent ruling (1106ms, 205606 gas)
    ✓ should not allow double automatic settlement for child milestones by parent ruling (2285ms, 38836 gas)
    ✓ should not allow invalid settlements (602ms, 23562 gas)
    ✓ should not allow to dispute settlement before it reaches the maturity date (2578ms, 394597 gas)
    ✓ should not allow to dispute valid settlement by non-party (2139ms, 121801 gas)
    ✓ should not allow to offer settlement by non-party (3090ms, 177064 gas)
    ✓ should allow settlement from payer (2482ms, 1375189 gas)
    ✓ should not allow internal resolution from random parties (1311ms, 322247 gas)
    ✓ should allow internal resolution from payee to payer (1224ms, 255877 gas)
    ✓ should allow internal resolution from payer to payee (1055ms, 530890 gas)
    ✓ should allow internal resolution from payee delegate to payer (1155ms, 531889 gas)
    ✓ should allow internal resolution from payer delegate to payee (1038ms, 529993 gas)
    ✓ should allow dispute with insurance full coverage (2977ms, 1341168 gas)
    ✓ should allow dispute with insurance partial coverage (3080ms, 1413445 gas)
    ✓ should allow dispute with insurance no coverage (3166ms, 1336446 gas)


### Contract: EscrowV1

    ✓ should allow registration of new contract (1109ms, 1680628 gas)
    ✓ should allow withdrawal if maturity by delivery deadline has been reached (468ms, 118718 gas)
    ✓ should not allow withdrawal if maturity was not reached (2613ms, 40068 gas)
    ✓ should not allow withdrawal if milestone is in dispute (5024ms, 1387509 gas)
    ✓ should not allow withdrawal if milestone dispute has failed and was restarted (3076ms, 163236 gas)
    ✓ should allow refund for matured by date milestone if dispute was won by Payer (3428ms, 1401030 gas)
    ✓ should not allow to cancel auto-release by random party (1531ms, 26326 gas)
    ✓ should allow to cancel auto-release by payer delegate (250ms, 14260 gas)
    ✓ should allow to cancel auto-release by payee delegate (312ms, 15372 gas)
    ✓ should allow to cancel auto-release by payer (282ms, 14708 gas)
    ✓ should allow to cancel auto-release by payee (242ms, 16269 gas)
    ✓ should allow amendments from payer (437ms, 149254 gas)
    ✓ should allow amendments from payee (1486ms, 126072 gas)
    ✓ should not allow legacy amendments (2823ms, 240885 gas)
    ✓ should not allow amendment approval from non-validator (958ms, 59563 gas)
    ✓ should not allow amendment approval for already approved amendments (1020ms, 111968 gas)
    ✓ should allow pre-approved amendments from anyone (408ms, 72502 gas)
    ✓ should allow pre-approved amendment from payer (563ms, 66790 gas)
    ✓ should allow pre-approved amendment from payee (760ms, 47566 gas)
    ✓ should allow milestone settlements (513ms, 346894 gas)
    ✓ should allow milestone settlements with partial changes (553ms, 211609 gas)
    ✓ should not allow milestone settlements with approval from invalid party (821ms, 183385 gas)
    ✓ should not allow milestone settlements with approval for invalid revision (2 instead of 3) (850ms, 72385 gas)
    ✓ should not allow milestone settlements with overfunding (4679ms, 210534 gas)
    ✓ should allow milestone settlements with overfunding if refund / release covers the margin (413ms, 122033 gas)
    ✓ should allow registration of new contract (459ms, 415086 gas)
    ✓ should not allow duplicate contracts (917ms, 28257 gas)
    ✓ should register additional milestone without amendment (342ms, 168074 gas)
    ✓ should register additional milestone with amendment (906ms, 234352 gas)
    ✓ should register additional child milestone (605ms, 164552 gas)
    ✓ should not allow duplicate milestones (1735ms, 28923 gas)
    ✓ should not allow milestones from non-party address (826ms, 27716 gas)
    ✓ should not allow funding of non-approved milestone (1767ms, 33875 gas)
    ✓ should allow funding of approved milestone (637ms, 275731 gas)
    ✓ should not allow over-funding of approved milestone (1620ms, 78235 gas)
    ✓ should fail funding of approved milestone if there is not enough funds (3137ms, 82132 gas)
    ✓ should allow partial funding (by 3rd party) of approved milestone (591ms, 143404 gas)
    ✓ should allow funding with payee signature (700ms, 299966 gas)
    ✓ should not allow withdrawal with a wrong amount with release signature from payer delegate (2669ms, 46616 gas)
    ✓ should not allow withdrawal with release signature for another milestone from payer delegate (2762ms, 46616 gas)
    ✓ should not allow withdrawal with release signature from another party (2211ms, 46628 gas)
    ✓ should not allow withdrawal over the funding with release signature from payer delegate (2883ms, 41537 gas)
    ✓ should allow withdraw with proper release signature from payer delegate (526ms, 101479 gas)
    ✓ should not allow refund with a wrong amount with release signature from payee (2863ms, 46661 gas)
    ✓ should not allow refund with release signature for another milestone from payee (2733ms, 46661 gas)
    ✓ should not allow refund with release signature from another party (2908ms, 46661 gas)
    ✓ should not allow refund over the funding with release signature from payee (2332ms, 41559 gas)
    ✓ should allow refund with proper release signature from payee (915ms, 86512 gas)
    ✓ should allow onchain cancelation from payee and refund claim from payer (525ms, 113185 gas)
    ✓ should not allow to change delegate by random (836ms, 26008 gas)
    ✓ should not allow to change delegate to empty address by payer (508ms, 22624 gas)
    ✓ should not allow to change delegate to empty address by payee (834ms, 22624 gas)
    ✓ should allow to change delegate by payer (220ms, 31893 gas)
    ✓ should allow to change delegate by payee (271ms, 31945 gas)
    ✓ should allow funding and withdraw by escrow contract from a treasury contract (1166ms, 539538 gas)
    ✓ should allow to execute scripts (598ms, 299261 gas)
    ✓ should not allow re-entrancy in executeSettlement (4230ms, 1163502 gas)
    ✓ should not allow re-entrancy in withdrawMilestone (4005ms, 1295057 gas)
    ✓ should not allow re-entrancy in withdrawPreApprovedMilestone (4932ms, 1241960 gas)
    ✓ should not allow re-entrancy in refundMilestone (5570ms, 1169523 gas)
    ✓ should not allow re-entrancy in refundPreApprovedMilestone (5730ms, 1243691 gas)
    ✓ should not allow mixed re-entrancy (7007ms, 1208799 gas)
    ✓ should not allow re-entrancy in executeSettlement with malicious reciever (4457ms, 2759077 gas)
    ✓ should not allow re-entrancy in withdrawMilestone with malicious reciever (1560ms, 1196558 gas)
    ✓ should not allow mixed re-entrancy in withdrawPreApprovedMilestone with malicious reciever (1930ms, 1265447 gas)
    ✓ should not allow re-entrancy in double funding with malicious sender (1507ms, 1089836 gas)
    ✓ should not allow malicious resolutions in executeSettlement (4642ms, 1074256 gas)

### Contract: Registry

    ✓ should allow to change insurance manager (214ms, 14135 gas)
    ✓ should not allow to change insurance manager by non-owner (506ms, 22766 gas)
